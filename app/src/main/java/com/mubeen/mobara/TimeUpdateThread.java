package com.mubeen.mobara;

public class TimeUpdateThread extends Thread {

    private static final String TAG = TimeUpdateThread.class.getName();
    private ActivityListener activityListener;


    public TimeUpdateThread(ActivityListener activityListener) {
        this.activityListener = activityListener;
    }


    @Override
    public void run() {
        super.run();
        try {
            while (true) {
                activityListener.updateTime(mUtils.getCurrentTime());
                Thread.sleep(60 * 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
