package com.mubeen.mobara;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mubeen.mobara.model.mAppDetails;

import java.lang.reflect.Type;
import java.util.List;

public class SPHandler {
    public static Type listType = new TypeToken<List<mAppDetails>>() {
    }.getType();
    private static SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MyAppClass.getContext());
    private static SharedPreferences.Editor editor = preferences.edit();

    public static void destroyPrefs() {
        preferences.edit().clear().commit();
    }


    public static List<mAppDetails> getConfigJson() {
        return new Gson().fromJson(preferences.getString("configJson", mConstant.DEFAULT_CONFIG_JSON), listType);
    }

    public static void setConfig(String configJson) {
        editor.putString("configJson", configJson);
        editor.apply();
    }

    public static mAppDetails getAppDetails() {
        if (preferences.getString("mAppDetails", "").equals("")) {
            return null;
        }
        return new Gson().fromJson(preferences.getString("mAppDetails", ""), mAppDetails.class);
    }

    public static void setAppDetails(mAppDetails mAppDetails) {
        editor.putString("mAppDetails", new Gson().toJson(mAppDetails));
        editor.apply();
    }

}
