package com.mubeen.mobara;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadHandler extends AsyncTask<String, Integer, String> {

    private static final String TAG = DownloadHandler.class.getName();
    private ActivityListener activityListener;
    private mEnum task;
    private Context mContext;
    private String fileLocation;
    private String fileName;


    public DownloadHandler(Context context, String fileLocation, String fileName, ActivityListener activityListener, mEnum task) {
        mContext = context;
        this.activityListener = activityListener;
        this.task = task;
        this.fileLocation = fileLocation;
        this.fileName = fileName;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.e(TAG, "onPreExecute run");
        switch (task) {
            case CONFIG_FILE:
                activityListener.showDialog("Please wait while we are getting updates...");
                break;
            case DOWNLOAD_APK:
                activityListener.showDownloadingProgress("Downloading...");
                break;
        }

    }

    @Override
    protected String doInBackground(String... sUrl) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            Log.e(TAG, "URL : " + url.toString());
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();

//            output = new FileOutputStream("/data/data/com.example.vadym.test1/textfile.txt");
            output = new FileOutputStream(fileLocation + "/" + fileName);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        Log.d(TAG, "onProgressUpdate: " + values[0]);
        switch (task) {
            case CONFIG_FILE:
                activityListener.showDialog("Downloading " + values[0]);
                break;
            case DOWNLOAD_APK:
                activityListener.showDownloadingProgress("Downloading (" + values[0] + "%)");
                break;
        }


    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        activityListener.fileDownloaded(task, fileLocation, fileName);
    }
}