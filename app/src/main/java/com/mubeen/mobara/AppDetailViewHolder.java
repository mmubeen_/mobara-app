package com.mubeen.mobara;

import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

class AppDetailViewHolder extends RecyclerView.ViewHolder {

    ImageView menu_img;

    public AppDetailViewHolder(View view) {
        super(view);
        menu_img = (ImageView) view.findViewById(R.id.menu_btn_img);
    }

    public ImageView getMenu_img() {
        return menu_img;
    }
}