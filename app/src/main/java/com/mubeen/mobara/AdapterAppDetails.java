package com.mubeen.mobara;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mubeen.mobara.model.mAppDetails;

import java.util.List;

public class AdapterAppDetails extends RecyclerView.Adapter<AppDetailViewHolder> {

    private List<mAppDetails> appDetailsList;
    private Context context;
    private ActivityListener activityListener;

    public AdapterAppDetails(Context context, ActivityListener activityListener, List<mAppDetails> appDetails) {
        this.context = context;
        this.appDetailsList = appDetails;
        this.activityListener = activityListener;
    }

    @Override
    public AppDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new AppDetailViewHolder(view);
    }


    @Override
    public void onBindViewHolder(AppDetailViewHolder holder, int position) {
        mAppDetails appDetails = appDetailsList.get(position);

        if (appDetails.getImagePath() != null) {
//            Glide.with(context).load(appDetails.getImagePath()).into(holder.menu_img);
            //loadImage(appDetails.getImagePath(),holder.menu_img);

            switch (appDetails.getName()){
                case "MobaraXtream":
                    holder.menu_img.setImageDrawable(context.getResources().getDrawable(R.drawable.mobara_extreme));
                    break;
                case "MobaraPlus":
                    holder.menu_img.setImageDrawable(context.getResources().getDrawable(R.drawable.mobara_plus));
                    break;
                case "MobaraXspeed":
                    holder.menu_img.setImageDrawable(context.getResources().getDrawable(R.drawable.mobara_speed));
                    break;

            }

        }

        holder.menu_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityListener.onMenuItemClick(appDetails);
            }
        });

    }

    @Override
    public int getItemCount() {
        return appDetailsList.size();
    }

    private void loadImage(String imageUrl, ImageView imageView) {
        Glide.with(context)
                .load(imageUrl)
                .into(imageView);
    }
}

