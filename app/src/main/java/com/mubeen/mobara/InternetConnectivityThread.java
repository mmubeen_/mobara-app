package com.mubeen.mobara;

public class InternetConnectivityThread extends Thread {

    private static final String TAG = InternetConnectivityThread.class.getName();
    private ActivityListener activityListener;


    public InternetConnectivityThread(ActivityListener activityListener) {
        this.activityListener = activityListener;
    }


    @Override
    public void run() {
        super.run();
        try {
            while (true) {
                activityListener.internetConnectionUpdate(mUtils.isNetworkAvailable(MyAppClass.getContext()));
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
