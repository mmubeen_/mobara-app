package com.mubeen.mobara;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mubeen.mobara.databinding.ActivityMainBinding;
import com.mubeen.mobara.model.mAppDetails;

import java.io.File;
import java.io.FileReader;
import java.util.List;

/*
 * Main Activity class that loads {@link MainFragment}.
 */
public class MainActivity extends FragmentActivity {

    private static final String TAG = MainActivity.class.getName();
    private final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    private final String PROVIDER_PATH = ".provider";
    private ActivityMainBinding binding;
    private TimeUpdateThread timeUpdateThread;
    private InternetConnectivityThread internetConnectivityThread;
    private ProgressDialog progressDialog;
    private AdapterAppDetails appDetailAdapter;
    private DownloadHandler downloadTask;
    private ActivityListener mainActivityListener = new ActivityListener() {


        @Override
        public void showDownloadingProgress(String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    binding.backBtn.setVisibility(View.GONE);
                    binding.progressTv.setText(message);
                }
            });
        }

        @Override
        public void onMenuItemClick(mAppDetails appDetails) {
            SPHandler.setAppDetails(appDetails);
            binding.downloadBtn.setEnabled(true);
            binding.menuGroup.setVisibility(View.GONE);
            binding.downloadGroup.setVisibility(View.VISIBLE);

            binding.progressTv.setText("");

            /*if (appDetails.getIsComingSoon()) {
                binding.downloadBtn.setImageDrawable(getDrawable(R.drawable.soon_btn));
            } else {
                binding.downloadBtn.setImageDrawable(getDrawable(R.drawable.download_btn));
            }
*/
            binding.downloadBtn.setImageDrawable(getDrawable(R.drawable.download_btn));

            if (appDetails.getDemoImagePath() != null) {
                /*Glide.with(MainActivity.this)
                        .load(appDetails.getDemoImagePath())
                        .into(binding.demoImageview);*/

//                loadImage(appDetails.getDemoImagePath(), binding.demoImageview);

                switch (appDetails.getName()) {
                    case "MobaraPlus":
                        binding.demoImageview.setImageDrawable(getResources().getDrawable(R.drawable.demo1));
                        break;
                    case "MobaraXspeed":
                        binding.demoImageview.setImageDrawable(getResources().getDrawable(R.drawable.demo2));
                        break;
                    case "MobaraXtream":
                        binding.demoImageview.setImageDrawable(getResources().getDrawable(R.drawable.demo3));
                        break;

                }
            }

            binding.downloadBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.e(TAG, "Download btn clicked");
                    if (appDetails.getIsComingSoon()) {
                        Toast.makeText(MainActivity.this, "The app will be available soon", Toast.LENGTH_SHORT).show();
                    } else {
                        int MyVersion = Build.VERSION.SDK_INT;
                        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                            if (!hasPermissions(PERMISSIONS)) {
                                requestForSpecificPermission();
                            } else {
                                proceedWithDownloading();
                            }
                        }


                    }

                }
            });
        }

        @Override
        public void showDialog(String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && !progressDialog.isShowing()) {
                        progressDialog.setMessage(message);
                        progressDialog.show();
                    } else {
                        progressDialog.setMessage(message);
                    }
                }
            });

        }

        @Override
        public void dismissDialog() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });

        }

        @Override
        public void updateTime(String time) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    binding.timeTv.setText(time);
                }
            });

        }

        @Override
        public void fileDownloaded(mEnum task, String fileLocation, String fileName) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        switch (task) {
                            case CONFIG_FILE:
                                try {
                                    dismissDialog();
                                    File file = new File(getFilesDir(), mConstant.CONFIG_FOLDER + "/config.json");
                                    JsonReader jsonReader = new JsonReader(new FileReader(file));
                                    List<mAppDetails> appDetailsList = new Gson().fromJson(jsonReader, SPHandler.listType);
                                    String json = new Gson().toJson(appDetailsList, SPHandler.listType);
                                    SPHandler.setConfig(json);
                                    renderViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    renderViews();
                                }

                                break;
                            case DOWNLOAD_APK:
                                Log.e(TAG, "APK downloaded");
                                binding.progressTv.setText("Installing...");
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    if (getPackageManager().canRequestPackageInstalls()) {
                                        installApk(fileLocation, fileName);
                                    } else {
                                        startActivityForResult(
                                                new Intent(android.provider.Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES,
                                                        Uri.parse("package:com.mubeen.mobara")), 456);
                                    }

                                } else {

                                    boolean isNonPlayAppAllowed = Settings.Secure.getInt(getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS) == 1;
                                    if (isNonPlayAppAllowed) {
                                        installApk(fileLocation, fileName);
                                    } else {
                                        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
                                        progressDialog.setTitle("Permission Required");
                                        progressDialog.setMessage("Please allow installation of apps from unknown sources");
                                        progressDialog.setCancelable(false);


                                        new AlertDialog.Builder(MainActivity.this)
                                                .setTitle("Permission Required")
                                                .setMessage("Please allow installation of apps from unknown sources")
                                                .setCancelable(false)
                                                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), 456);
                                                    }
                                                })
                                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                        goBack();
                                                    }
                                                })
                                                .show();
                                    }

                                }

                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        }

        @Override
        public void internetConnectionUpdate(boolean status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (status) {
                        binding.wifiLogo.setVisibility(View.VISIBLE);
                    } else {
                        binding.wifiLogo.setVisibility(View.GONE);
                    }
                }
            });

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            switch (requestCode) {
                case 123:
                    if (resultCode == RESULT_OK) {
                        Log.e(TAG, "Install success");
                        Toast.makeText(this, "App successfully installed", Toast.LENGTH_SHORT).show();
                        goBack();
                    } else if (resultCode == RESULT_CANCELED) {
                        Log.e(TAG, "Install cancelled");
                        Toast.makeText(this, "You have cancelled installation", Toast.LENGTH_SHORT).show();
                        goBack();
                    } else {
                        Log.e(TAG, "Install not success");
                        Toast.makeText(this, "Failed to install app", Toast.LENGTH_SHORT).show();
                        goBack();
                    }
                    break;
                case 456:
                    if (SPHandler.getAppDetails() == null) {
                        Log.e(TAG, "AppDetail is null");
                    }
                    File apk_directory = new File(getExternalFilesDir(mConstant.LOCAL_PARENT_FOLDER), mConstant.APKS_FOLDER);

                    String fileName = SPHandler.getAppDetails().getName().trim() + ".apk";

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (getPackageManager().canRequestPackageInstalls()) {
                            installApk(apk_directory.getAbsolutePath(), fileName);
                        } else {
                            Toast.makeText(this, "Permission denied!", Toast.LENGTH_SHORT).show();
                            goBack();
                        }
                    } else {
                        boolean isNonPlayAppAllowed = Settings.Secure.getInt(getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS) == 1;
                        if (isNonPlayAppAllowed) {
                            installApk(apk_directory.getAbsolutePath(), fileName);
                        } else {
                            Toast.makeText(this, "Permission denied!", Toast.LENGTH_SHORT).show();
                            goBack();
                        }
                    }

                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void goBack() {
        binding.downloadGroup.setVisibility(View.GONE);
        binding.menuGroup.setVisibility(View.VISIBLE);
    }

    private void installApk(String fileLocation, String fileName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri contentUri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + PROVIDER_PATH,
                    new File(fileLocation + "/" + fileName));
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            install.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            install.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
            install.putExtra(Intent.EXTRA_RETURN_RESULT, true);
            install.setData(contentUri);
            startActivityForResult(install, 123);
        } else {
            Uri apkUri = Uri.fromFile(new File(fileLocation, fileName));
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
            intent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
            startActivityForResult(intent, 123);
        }
    }

    private void proceedWithDownloading() {
        if (mUtils.isNetworkAvailable(MainActivity.this)) {
            binding.downloadBtn.setImageDrawable(getDrawable(R.drawable.download_btn_disabled));
            binding.downloadBtn.setEnabled(false);
            startDownloadingTask(mEnum.DOWNLOAD_APK, SPHandler.getAppDetails());
        } else {
            Toast.makeText(MainActivity.this, "No internet connectivity", Toast.LENGTH_SHORT).show();
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        startTimeThread();
        startInternetConnectivityThread();

        emptyApkFolder();

        if (SPHandler.getConfigJson().size() > 1) {
            renderViews();
        } else {
            //Configuration is not set.
            //Downloading config file
//            startDownloadingTask(mEnum.CONFIG_FILE);
        }

        /*binding.refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDownloadingTask(mEnum.CONFIG_FILE);
            }
        });*/

        binding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.downloadGroup.setVisibility(View.GONE);
                binding.menuGroup.setVisibility(View.VISIBLE);
            }
        });

    }

    private void emptyApkFolder() {

        File apk_directory = new File(getExternalFilesDir(mConstant.LOCAL_PARENT_FOLDER), mConstant.APKS_FOLDER);
//        File apk_directory = new File(Environment.getExternalStorageDirectory() + mConstant.LOCAL_PARENT_FOLDER, mConstant.APKS_FOLDER);
        if (apk_directory.exists()) {
            mUtils.deleteRecursive(apk_directory);
        }
    }

    private void stopDownloadingTask() {
        if (downloadTask != null &&
                (downloadTask.getStatus() == AsyncTask.Status.PENDING
                        || downloadTask.getStatus() == AsyncTask.Status.RUNNING)) {
            downloadTask.cancel(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopDownloadingTask();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopDownloadingTask();
    }

    private void renderViews() {

        try {

            appDetailAdapter = new AdapterAppDetails(this, mainActivityListener, SPHandler.getConfigJson());
            binding.menuRecycler.setAdapter(appDetailAdapter);
            binding.menuRecycler.setLayoutManager(new LinearLayoutManager(this));

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void startDownloadingTask(mEnum task) {
        startDownloadingTask(task, null);
    }

    private void startDownloadingTask(mEnum task, mAppDetails appDetails) {

        switch (task) {
            case CONFIG_FILE:
                File config_directory = new File(getFilesDir(), mConstant.CONFIG_FOLDER);
                if (!config_directory.exists()) {
                    config_directory.mkdirs();
                }
                downloadTask = new DownloadHandler(this, config_directory.getAbsolutePath(), "config.json", mainActivityListener, mEnum.CONFIG_FILE);
                downloadTask.execute(mConstant.CONFIG_URL);
                break;
            case DOWNLOAD_APK:

                try {
                    File apk_directory = new File(getExternalFilesDir(mConstant.LOCAL_PARENT_FOLDER), mConstant.APKS_FOLDER);

                    Log.e(TAG, "Apk directory : " + apk_directory.getAbsolutePath());
                    if (!apk_directory.exists()) {
                        apk_directory.mkdirs();
                    }


                    downloadTask = new DownloadHandler(this, apk_directory.getAbsolutePath(), appDetails.getName().trim() + ".apk", mainActivityListener, mEnum.DOWNLOAD_APK);
                    downloadTask.execute(appDetails.getApkPath());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }


    }

    private void startInternetConnectivityThread() {
        if (internetConnectivityThread != null && internetConnectivityThread.isAlive()) {
            internetConnectivityThread.interrupt();
        }
        internetConnectivityThread = new InternetConnectivityThread(mainActivityListener);
        internetConnectivityThread.setName("Internet_Thread");
        internetConnectivityThread.setDaemon(true);
        internetConnectivityThread.start();
    }

    private void startTimeThread() {
        if (timeUpdateThread != null && timeUpdateThread.isAlive()) {
            timeUpdateThread.interrupt();
        }
        timeUpdateThread = new TimeUpdateThread(mainActivityListener);
        timeUpdateThread.setName("TimeUpdate_Thread");
        timeUpdateThread.setDaemon(true);
        timeUpdateThread.start();
    }

    private boolean hasPermissions(String... permissions) {
        if (permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(MyAppClass.getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        proceedWithDownloading();
                    }
                } else {
                    Toast.makeText(this, "Permission Not granted", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void loadImage(String imageUrl, ImageView imageView) {
        try {
            Glide.with(MainActivity.this)
                    .load(imageUrl)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {

        if (binding.downloadGroup.getVisibility() == View.VISIBLE) {
            goBack();
        } else {
            super.onBackPressed();
        }
    }
}