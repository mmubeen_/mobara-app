package com.mubeen.mobara;

import com.mubeen.mobara.model.mAppDetails;

public interface ActivityListener {
    void updateTime(String time);

    void internetConnectionUpdate(boolean status);

    void fileDownloaded(mEnum task, String fileLocation, String fileName);

    void showDialog(String message);

    void dismissDialog();

    void onMenuItemClick(mAppDetails appDetails);

    void showDownloadingProgress(String message);
}
