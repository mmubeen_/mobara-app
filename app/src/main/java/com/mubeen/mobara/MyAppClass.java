package com.mubeen.mobara;

import android.app.Application;
import android.content.Context;

public class MyAppClass extends Application {
    public static MyAppClass instance;

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
