package com.mubeen.mobara.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class mAppDetails {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("apk_path")
    @Expose
    private String apkPath;
    @SerializedName("image_path")
    @Expose
    private String imagePath;
    @SerializedName("demo_image_path")
    @Expose
    private String demoImagePath;
    @SerializedName("is_coming_soon")
    @Expose
    private Boolean isComingSoon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApkPath() {
        return apkPath;
    }

    public void setApkPath(String apkPath) {
        this.apkPath = apkPath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDemoImagePath() {
        return demoImagePath;
    }

    public void setDemoImagePath(String demoImagePath) {
        this.demoImagePath = demoImagePath;
    }

    public Boolean getIsComingSoon() {
        return isComingSoon;
    }

    public void setIsComingSoon(Boolean isComingSoon) {
        this.isComingSoon = isComingSoon;
    }


}
