package com.mubeen.mobara;

public class mConstant {
    public static final String LOCAL_PARENT_FOLDER = "Mobara";
    public static final String CONFIG_FOLDER = LOCAL_PARENT_FOLDER + "/config";
    public static final String APKS_FOLDER = "/apks/";


    public static final String CONFIG_URL = "http://62.210.82.209/AromaTV/Aroma%20Config/Aroma_config.txt";
    public static final String DEFAULT_CONFIG_JSON = "[{\"name\":\"MobaraPlus\",\"apk_path\":\"https://mobara.tv/apk/mobara-2.3.apk\",\"image_path\":\"none\",\"demo_image_path\":\"none\",\"is_coming_soon\":false},{\"name\":\"MobaraXspeed\",\"apk_path\":\"https://mobara.tv/apk/mspeedx4.apk\",\"image_path\":\"none\",\"demo_image_path\":\"none\",\"is_coming_soon\":false},{\"name\":\"MobaraXtream\",\"apk_path\":\"https://mobara.tv/apk/mob2.1.2.apk\",\"image_path\":\"none\",\"demo_image_path\":\"none\",\"is_coming_soon\":false}]";
}
